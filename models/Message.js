
const mongoose = require('mongoose');
const { Schema } = mongoose;

const messageSchema = new Schema({
  text: {
    type: String,
    required: true,
    validate: {
        validator: function (v) {
            return /^[a-zA-Z0-9-_ ]+$/.test(v);
        },
        message: '{PATH} must have letters only!'
          },
    },
  userId: {
    type: String,
    required: true,
    validate: {
      validator: function (v) {
          return /^[a-zA-Z0-9-_]+$/.test(v);
      },
      message: '{PATH} must have letters only!'
        },
    },
    timestamp: {
        type: Date,
        required: true,
        default: Date.now,
        },
});

const Message = mongoose.model('Message', messageSchema); 

export default Message;