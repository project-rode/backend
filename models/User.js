
const mongoose = require('mongoose');
const { Schema } = mongoose;
import bcrypt from 'bcrypt';

const userSchema = new Schema({
  username: {
      type: String,
      required: true,
      unique: true,
      validate: {
        validator: function (v) {
            return /^[a-zA-Z0-9-_]+$/.test(v);
        },
        message: '{PATH} must have letters only!'
    },
  },
  email: {
        type: String,
        required: true,
        unique: true,
        validate: {
        validator: function (v) {
            return /^[a-zA-Z0-9-_@.]+$/.test(v);
        },
        message: '{PATH} must have letters only!'
    },
  },
  password: {
        type: String,
        required: true,
        minLength: 7,
        maxLength: 42,
        validate: {
        validator: function (v) {
            return /^[a-zA-Z0-9-_]+$/.test(v);
        },
        message: '{PATH} must have letters only!'
    },
  },
  role: {
      type: String,
  }
});

userSchema.statics.findByLogin = async function(login) {
    return User.findOne({username: login}, function(err, userObj){
        if(err){
            console.log(err)
        } else if(userObj){
            return userObj
        } else {
            return null;
        }
    });
  };

userSchema.pre("save", async function(next) {
    this.password = await User.generatePasswordHash(this.password);
    next();
})

userSchema.statics.generatePasswordHash = async function(password) {
    const saltRounds = 10;
    return await bcrypt.hash(password, saltRounds);
}

userSchema.methods.validatePassword = async function(password){
    return await bcrypt.compare(password, this.password);
}

const User = mongoose.model('User', userSchema); 




export default User;