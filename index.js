import express from 'express';
import jwt from 'jsonwebtoken';
import { ApolloServer, AuthenticationError } from 'apollo-server-express';
import cors from 'cors';
import mongoose from 'mongoose';
import depthLimit from 'graphql-depth-limit'
import http from 'http';
require('dotenv').config();

import schema from './schema';
import resolvers from './resolvers';
import models from './models';

const app = express();
app.use(cors());

mongoose.connect('mongodb://root:Dy6YJFuqq4H93cDz@' + process.env.HOST + ':27017/rode', { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true });
mongoose.connection.once('open', () => {
  console.log(`Connected to: MongoDB ${process.env.HOST}:27017/rode`);
});

const getMe = req => {
  const token = req.headers['x-token'];
  if (token) {
    try {
      return jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
      throw new AuthenticationError(
        'Your session expired. Sign in again.',
      );
    }
  }
};

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  context: async({req, connection}) => {
    if (connection) {
      return {
        ...models
      };
    }
    if (req) {
    const me = await getMe(req);
    return {
      ...models,
      me,
      secret: process.env.JWT_SECRET,
    }
  }
  },
  validationRules: [depthLimit(4)]
});

server.applyMiddleware({ app, path: '/graphql' });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port: process.env.PORT }, () => {
  console.log("Apollo Server on http://localhost:" + process.env.PORT + "/graphql");
});