import { ForbiddenError } from 'apollo-server';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isMessageOwner } from './authorization';
import pubsub, { EVENTS } from '../subscription';
export default {
  Query: {
    messages: (parent, {offset = 0, limit = 100}, { Message }) => {
        return Message.find({})
                .skip(offset)
                .limit(limit)
      },
      message: (parent, { id }, { Message }) => {
        return Message.findById(id)
      },
  },

  Mutation: {
    createMessage: combineResolvers(
        isAuthenticated,
        (parent, { text }, { Message, me }) => {
        const message = new Message ({
          text,
          userId: me._id,
        });

        pubsub.publish(EVENTS.MESSAGE.CREATED, {
            messageCreated: { message },
          });
          
        return message.save();
      },
    ),
    deleteMessage: combineResolvers(
        isAuthenticated,
        isMessageOwner,
        async(parent, { id }, { Message }) => {
        return await Message.findOneAndDelete({_id: id}, {maxTimeMS: 5})
         .then(resp => {
            if (resp) {
            console.log('Deleted a document', resp);
            return true;
            }
            return false;
         })
         .catch(err => {
            console.log(err);
            return false;
         })
 
    },
    ),
  updateMessage: (parent, { id, text, userId }, { Message }) => {
    const message = new Message({
        text,
        userId
    });
    const upsertData = message.toObject();
    delete upsertData._id;
    return Message.findOneAndUpdate({ _id: id}, { $set: upsertData }, {new: true});

  },
  },

  Message: {
    user: (parent, args, { User }) => {
        return User.findById(parent.userId);
      },
  },
  Subscription: {
    messageCreated: {
      subscribe: () => pubsub.asyncIterator(EVENTS.MESSAGE.CREATED),
    },
  },
};