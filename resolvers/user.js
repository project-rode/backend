import jwt from 'jsonwebtoken';
import { combineResolvers } from 'graphql-resolvers';
import { isAdmin } from './authorization';
import { AuthenticationError, UserInputError } from 'apollo-server';

const createToken = (user, secret, expiresIn) => {
    const { _id, email, username, role } = user;
    return jwt.sign({ _id, email, username, role }, secret, {
        expiresIn,
    });
  };

export default {
    Query: {
        users: (parent, args, { User }) => {
            return User.find({});
          },
          user: (parent, { id }, { User }) => {
              console.log(User.findById(id));
            return User.findById(id)
          },
          me: (parent, args, { User, me }) => {
            if (!me) {
                return null;
            }
            return User.findById(me._id)
          },
    },
    Mutation: {
        signUp: (parent, { username, email, password, role }, { User, secret }) => {
            const user = new User ({
              username,
              email,
              password,
              role
            });
            user.save();
            return { token: createToken(user, secret, '30m') };
          },
        signIn: async (
            parent,
            { login, password },
            { User, secret },
          ) => {
            const user = await User.findByLogin(login);
      
            if (!user) {
              throw new UserInputError(
                'No user found with this login credentials.',
              );
            }
      
            const isValid = await user.validatePassword(password);
      
            if (!isValid) {
              throw new AuthenticationError('Invalid password.');
            }
            return { token: createToken(user, secret, '30m') };
          },
          deleteUser: combineResolvers(
              isAdmin,
              async (parent, { id }, { User }) => {
            return await User.findOneAndDelete({_id: id}, {maxTimeMS: 5})
         .then(resp => {
            if (resp) {
            console.log('Deleted a document', resp);
            return true;
            }
            return false;
         })
         .catch(err => {
            console.log(err);
            return false;
         })
          },
          ),
    },

    User: {
        messages: (user, args, { Message }) => {
            return Message.find({userId: user._id})
          },
    },
  };